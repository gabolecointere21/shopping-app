# shopping-app

### LIVE DEMO
https://relaxed-ardinghelli-fd4cef.netlify.com/

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### LIVE DEMO
https://relaxed-ardinghelli-fd4cef.netlify.com/
