// credits: took the idea from the built in vuex recorder: https://github.com/vuejs/vuex/blob/dev/src/plugins/logger.js
export default ({ events = [], recorder = console }) => (store) => {
  // by acepting recorder as a parameter, we can write multiple implementations of it
  // in order to record on the database, Google Analytics, Console, or whatever we prefer
  store.subscribe((mutation, state) => {
    // if no events array was provided, record all type of mutations
    // else, only record the mutation types provided in the array
    if (events.length < 1 || events.indexOf(mutation.type) >= 0) {
      if (typeof recorder.log !== 'function') {
        // Ideally I would define an Interface the recorder needs to implement
        // Invalid recorder provided, recorder.log must be a function
        return;
      }

      const record = {
        event: {
          datetime: new Date().toLocaleString(),
          type: mutation.type,
          state: state.cart,
        },
      };

      // Log to where the recorder was implemented to, default: the console
      recorder.log(record);
    }
  });
};
