import Vue from 'vue';
import VueRouter from 'vue-router';
import ItemList from '../views/ItemList.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'item-list',
    component: ItemList,
  },
  {
    path: '/shopping-cart',
    name: 'shopping-cart',
    // route level code-splitting
    // this generates a separate chunk (shopping-cart.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "shopping-cart" */ '../views/ShoppingCart.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
