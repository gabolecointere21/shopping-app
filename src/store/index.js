import Vue from 'vue';
import Vuex from 'vuex';
import EventRecorder from '../plugins/event-recorder';

import item from './modules/item.module';
import cart from './modules/cart.module';
import notification from './modules/notification.module';

Vue.use(Vuex);

// array containing the type of mutations we want to record
const eventsToRecord = ['addItemToCart', 'removeItemFromCart', 'removeItemFromCart', 'incrementCartItemQuantity', 'decreaseCartItemQuantity', 'emptyCart', 'applyValidDiscount', 'removeDiscountApplied'];

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    item,
    cart,
    notification,
  },
  plugins: [EventRecorder({
    events: eventsToRecord,
  })],
});
