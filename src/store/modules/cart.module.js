const initialState = {
  cartItems: [],
  discount: {
    valid: false,
  },
};

// const MAX_ALLOWED_QUANTITY = 1000;

export default {
  state: { ...initialState },

  mutations: {
    addItemToCart(state, item) {
      // add new item to the shopping cart
      state.cartItems.push(item);
    },
    removeItemFromCart(state, index) {
      // remove an item from the shopping cart
      state.cartItems.splice(index, 1);
    },
    incrementCartItemQuantity(state, index) {
      // increment the quantity of an existing item in the cart
      state.cartItems[index].quantity += 1;
    },
    decreaseCartItemQuantity(state, index) {
      // increment the quantity of an existing item in the cart
      state.cartItems[index].quantity -= 1;
    },
    emptyCart(state) {
      // empty cart
      state.cartItems = [];
    },
    applyValidDiscount(state, validDiscount) {
      // add valid discount
      state.discount = {
        ...validDiscount,
        valid: true,
      };
    },
    removeDiscountApplied(state) {
      state.discount = {
        valid: false,
      };
    },
  },

  actions: {
    addToCart({ commit, state }, item) {
      // check if the item is already in the cart or not
      const indexOfExistingItem = state.cartItems.findIndex(cartItem => cartItem.sku === item.sku);
      // if this item doesn't exist yet in the cart
      // add it to the array of existing items
      if (indexOfExistingItem < 0) {
        const itemToAdd = {
          ...item,
          quantity: 1,
        };
        commit('addItemToCart', itemToAdd);

        // if this item already exists in the cart
        // increment the quantity property
      } else {
        commit('incrementCartItemQuantity', indexOfExistingItem);
      }
    },
    decreaseCartItemQuantity({ commit, state }, item) {
      // decrease quantity of an item
      const indexOfExistingItem = state.cartItems.findIndex(cartItem => cartItem.sku === item.sku);

      // decrease by one if quantity > 1, else remove item
      const mutationToCommit = item.quantity > 1 ? 'decreaseCartItemQuantity' : 'removeItemFromCart';

      commit(mutationToCommit, indexOfExistingItem);
    },
    removeItemFromCart({ commit, state }, item) {
      // remove item from Cart
      const indexOfExistingItem = state.cartItems.findIndex(cartItem => cartItem.sku === item.sku);

      commit('removeItemFromCart', indexOfExistingItem);

      if (!state.cartItems.length && state.discount.valid) {
        commit('removeDiscountApplied');
      }
    },
    emptyCart({ commit, state }) {
      // empty cart
      commit('emptyCart');
      // if applied, remove discount
      if (state.discount.valid) {
        commit('removeDiscountApplied');
      }
    },
    applyValidDiscount({ commit }, validDiscount) {
      // add discount object to state
      commit('applyValidDiscount', validDiscount);
    },
  },
};
