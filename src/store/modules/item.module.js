const initialState = {
  items: [
    {
      sku: 'SKU-ITEM-001',
      display_name: 'ITEM 001',
      price: 12,
    },
    {
      sku: 'SKU-ITEM-002',
      display_name: 'ITEM 002',
      price: 15,
    },
    {
      sku: 'SKU-ITEM-003',
      display_name: 'ITEM 003',
      price: 19,
    },
    {
      sku: 'SKU-ITEM-004',
      display_name: 'ITEM 004',
      price: 22,
    },
    {
      sku: 'SKU-ITEM-005',
      display_name: 'ITEM 005',
      price: 9,
    },
    {
      sku: 'SKU-ITEM-006',
      display_name: 'ITEM 006',
      price: 14,
    },
    {
      sku: 'SKU-ITEM-007',
      display_name: 'ITEM 007',
      price: 23,
    },
    {
      sku: 'SKU-ITEM-008',
      display_name: 'ITEM 008',
      price: 12,
    },
    {
      sku: 'SKU-ITEM-009',
      display_name: 'ITEM 09',
      price: 11,
    },
  ],
};

export default {
  state: { ...initialState },
};
