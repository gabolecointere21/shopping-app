const initialState = {
  timeout: 3000,
  text: '',
  snackbar: false,
};

export default {
  state: { ...initialState },

  mutations: {
    // display the notification
    showNotification(state) {
      state.snackbar = true;
    },
    // modify notification text
    modifyNotificationText(state, payload) {
      state.text = payload;
    },
    // set notif timeout
    modifyNotificationTimeout(state, payload) {
      state.timeout = payload;
    },
    // hide snackbar
    hideNotification(state) {
      state.snackbar = false;
    },
  },

  actions: {
    showNotification({ commit }, payload) {
      if (typeof payload.text !== 'undefined') {
        commit('modifyNotificationText', payload.text);
      }

      if (typeof payload.timeout !== 'undefined') {
        commit('modifyNotificationTimeout', payload.timeout);
      }

      commit('showNotification');
    },
    hideNotification({ commit }) {
      // reset to nitial values
      commit('modifyNotificationText', initialState.text);
      commit('modifyNotificationTimeout', initialState.timeout);

      // hide notification
      commit('hideNotification');
    },
  },
};
